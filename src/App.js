import React, { Component, Fragment } from 'react'
import './App.css'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import Feed from './components/Feed/Feed'
import Preloader from './components/Preloader/Preloader'
import { Route, Switch } from 'react-router-dom'
import Menu from './components/Menu/Menu'

class App extends Component {
  constructor (props) {
    super(props)

    console.log('constructor')
    this.state = {
      name: 'Andrew',
      age: 30
    }
  }

  componentWillMount () {
    console.log('componentWillMount')

    setTimeout(this.fetchDataFromServer.bind(this), 1000)
  }

  fetchDataFromServer () {
    fetch('/feed.json')
      .then(res => res.json())
      .then(res => this.setState({
        news: res
      }))
  }

  render () {
    console.log('render')

    if (!this.state.news) {
      return <Preloader/>
    }

    return (
      <Fragment>
        <Menu/>

          <Route path='/header'
                 render={(props) => <Header {...props}
                                            name={this.state.name}
                                            age={this.state.age}/>}/>

          <Route path='/header'
                 render={(props) => <Feed {...props} news={this.state.news}/>}/>
          <Route exact path='/footer' component={Footer}/>

      </Fragment>
    )
  }
}

export default App
