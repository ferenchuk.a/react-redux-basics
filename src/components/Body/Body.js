import React, { Component } from 'react'
import logo from '../../logo.svg';

class Body extends Component {
  render() {
    const bodyHeader = (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
    )

    const menuItems = [1, 2, 3, 4, 5]
      .map(num => <li key={num}>Item {num}</li>)

    const menu = (
      <ul>
        {menuItems}
      </ul>
    )

    const userChoice = true

    return (
      <div className="App">
        {menu}
        <h1>Hello world!!!</h1>
        {userChoice && <h2>Block 1</h2>}
        {!userChoice && <h2>Block 2</h2>}
        {bodyHeader}
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    )
  }
}

export default Body