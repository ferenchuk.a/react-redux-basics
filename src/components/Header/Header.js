import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

class Header extends Component {
  render () {
    const {name, age, likes} = this.props

    return (
      <Fragment>
        <h1>Hello, {name}</h1>
        <h2>Your age is {age}</h2>
        <h3>Likes: {likes}</h3>
      </Fragment>
    )
  }
}

const mapStoreToProps = (store) => {
  return {
    likes: store.likes
  }
}

export default connect(mapStoreToProps)(Header)